package com.examplerepo.repositories;

import com.example.models.User;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {

    @Select("select * from tb_user")
    List<User> getAll();

}
